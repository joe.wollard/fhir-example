import {appReducer} from './reducers/app'
import {createStore} from 'redux'
import {initialState} from './initialState'

export const store = createStore(
  appReducer,
  initialState,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

if (module.hot) {
  module.hot.accept('./reducers', () => {
    const nextRootReducer = require('./reducers/app')
    store.replaceReducer(nextRootReducer)
  })
}
