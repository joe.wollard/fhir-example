export const types = {
  SET_CONDITIONS_ERROR: 'SET_CONDITIONS_ERROR',
  SET_CONDITIONS_LOADING: 'SET_CONDITIONS_LOADING',
  SET_CONDITIONS: 'SET_CONDITIONS',
  SET_ERROR: 'SET_ERROR',
  SET_LOADING: 'SET_LOADING',
  SET_PATIENT_DATA: 'SET_PATIENT_DATA'
}
