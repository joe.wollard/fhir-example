export const initialState = {
  conditions: null,
  conditionsError: null,
  conditionsLoading: false,
  error: null,
  loading: false,
  patientData: null
}
