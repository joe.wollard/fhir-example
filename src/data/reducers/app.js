import {types} from '../types'

export function appReducer (state, action) {
  switch (action.type) {
    case types.SET_CONDITIONS:
      return {
        ...state,
        conditions: action.data,
        conditionsError: null
      }

    case types.SET_CONDITIONS_LOADING:
      return {
        ...state,
        conditionsLoading: action.data
      }

    case types.SET_CONDITIONS_ERROR:
      return {
        ...state,
        conditionsError: action.data
      }

    case types.SET_PATIENT_DATA:
      return {
        ...state,
        patientData: action.data,
        error: null
      }

    case types.SET_LOADING:
      return {
        ...state,
        loading: action.data
      }

    case types.SET_ERROR:
      return {
        ...state,
        error: action.data
      }

    default:
      return state
  }
}
