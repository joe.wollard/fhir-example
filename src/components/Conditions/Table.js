import BoostrapTable from 'react-bootstrap-table-next'
import React, {Component} from 'react'
import './Table.css'

export class ConditionsTable extends Component {
  render () {
    const columns = [{
      dataField: 'resource.code.text',
      text: 'Condition',
      sort: true,
      formatter: (cell, row) => {
        return (
          <a
            target='nih'
            href={`https://www.ncbi.nlm.nih.gov/pubmed/?term=${cell}`}
          >
            {cell}
          </a>
        )
      }
    }, {
      dataField: 'resource.dateRecorded',
      text: 'Date Recorded',
      sort: true,
      headerStyle: (col, colIdx) => {
        return {minWidth: '14rem'}
      },
      formatter: (cell, row) => cell ? new Date(cell).toLocaleString() : '??'
    }]

    return (
      <div style={{maxHeight: '90vh', overflow: 'auto'}}>
        {this.props.error && this.props.error.toString()}
        {this.props.loading && ' loading...'}
        {(this.props.conditions.length > 0 && !this.props.loading) &&
          <BoostrapTable
            keyField='id'
            data={this.props.conditions}
            columns={columns}
            defaultSorted={[{
              dataField: 'resource.dateRecorded',
              order: 'desc'
            }]}
          />
        }
        {(!this.props.conditions.length && !this.props.loading) &&
          <div className='text-center lead text-muted'>
            No conditions found
          </div>
        }
      </div>
    )
  }
}
