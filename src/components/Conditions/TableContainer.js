import {ConditionsTable} from './Table'
import {connect} from 'react-redux'

function mapStateToProps (state) {
  const {patientData} = state
  let resource = {}
  if (patientData && patientData.entry && patientData.entry[0]) {
    resource = patientData.entry[0].resource
  }

  let conditions = []
  if (state.conditions && state.conditions.entry) {
    conditions = state.conditions.entry
  }

  console.log(state.conditionsLoading)
  return {
    conditions,
    loading: state.conditionsLoading,
    error: state.conditionsError,
    patientId: resource.id
  }
}

export const ConditionsTableContainer = connect(mapStateToProps)(ConditionsTable)
