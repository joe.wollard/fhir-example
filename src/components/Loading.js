import React from 'react'
import ReactLoading from 'react-loading'

export function Loading (props) {
  return (
    <div className='text-center'>
      <ReactLoading
        type='cylon'
        height={400}
        style={{fill: '#e9711c', margin: 'auto', width: '100px'}}
        {...props}
      />
    </div>
  )
}
