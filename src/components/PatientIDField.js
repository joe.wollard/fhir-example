import {Input, FormText, FormGroup} from 'reactstrap'
import FHIR from '../lib/FHIR'
import PropTypes from 'prop-types'
import React, {Component} from 'react'

export class PatientIDField extends Component {
  constructor (props) {
    super(props)
    this.state = {
      patientRecord: null
    }
  }

  fetchFHIRData (patientId) {
    this.props.setLoading(true)
    FHIR.getPatientInfo(patientId, (err, data) => {
      this.props.setLoading(false)
      if (err) {
        this.props.setError(err)
      } else {
        this.props.setPatientData(data)
      }
    })

    this.props.setConditionsLoading(true)
    FHIR.getPatientConditions(patientId, (err, data) => {
      this.props.setConditionsLoading(false)
      if (err) {
        this.props.setConditionsError(err)
      } else {
        this.props.setConditions(data)
      }
    })
  }

  findPatientRecord (e) {
    const value = e.target.value
    window.clearTimeout(this.searchThrottleTimeout)
    this.searchThrottleTimeout = window.setTimeout(() => {
      this.fetchFHIRData(value)
    }, 500)
  }

  render () {
    return (
      <FormGroup>
        <Input
          type='text'
          placeholder='Patient ID'
          onChange={this.findPatientRecord.bind(this)}
          disabled={this.props.loading}
        />
        <FormText color='muted'>
          Enter a patient ID to search for related conditions.
        </FormText>
      </FormGroup>
    )
  }
}

PatientIDField.propTypes = {
  setError: PropTypes.func,
  setLoading: PropTypes.func,
  setPatientData: PropTypes.func
}
