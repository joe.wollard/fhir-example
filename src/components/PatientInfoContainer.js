import {connect} from 'react-redux'
import {PatientInfo} from './PatientInfo'

function mapStateToProps (state) {
  const {patientData} = state
  let resource = {name: [{}]}
  if (patientData.entry && patientData.entry[0]) {
    resource = patientData.entry[0].resource
  }

  return {
    dob: new Date(resource.birthDate),
    name: resource.name[0].text,
    gender: resource.gender
  }
}

export const PatientInfoContainer = connect(mapStateToProps)(PatientInfo)
