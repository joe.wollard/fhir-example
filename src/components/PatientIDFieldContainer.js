import {connect} from 'react-redux'
import {PatientIDField} from './PatientIDField'
import {store} from '../data/store'
import {types} from '../data/types'

function mapStateToProps (state) {
  return {
    loading: state.loading,
    conditionsLoading: state.conditionsLoading,

    setConditionsLoading: function (isLoading) {
      store.dispatch({type: types.SET_CONDITIONS_LOADING, data: isLoading})
    },

    setLoading: function (isLoading) {
      store.dispatch({type: types.SET_LOADING, data: isLoading})
    },

    setError (err) {
      store.dispatch({type: types.SET_ERROR, data: err})
    },

    setConditionsError (err) {
      store.dispatch({type: types.SET_CONDITIONS_ERROR, data: err})
    },

    setPatientData (data) {
      store.dispatch({type: types.SET_PATIENT_DATA, data: data})
    },

    setConditions (data) {
      store.dispatch({type: types.SET_CONDITIONS, data: data})
    }
  }
}

export const PatientIDFieldContainer = connect(mapStateToProps)(PatientIDField)
