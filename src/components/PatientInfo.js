import React from 'react'
import {Card, CardHeader, CardBody} from 'reactstrap'
import PropTypes from 'prop-types'

export function PatientInfo ({name, gender, dob, ...props}) {
  return (
    <React.Fragment>
      {name &&
        <Card>
          <CardHeader>Patient Info</CardHeader>
          <CardBody>
            <dl>
              <dt>Name</dt>
              <dd>{name}</dd>
              <dt>Gender</dt>
              <dd>{gender}</dd>
              <dt>Date of Birth</dt>
              <dd>{dob.toLocaleDateString()}</dd>
            </dl>
          </CardBody>
        </Card>
      }
      {!name &&
        <div className='text-center lead text-muted'>
          No matching patient found
        </div>
      }
    </React.Fragment>
  )
}

PatientInfo.propTypes = {
  name: PropTypes.string,
  gender: PropTypes.string,
  dob: PropTypes.instanceOf(Date)
}

PatientInfo.defaultProps = {
  name: '',
  gender: '',
  dob: new Date()
}
