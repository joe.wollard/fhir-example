import './polyfills'
import 'bootstrap/dist/css/bootstrap.css'
import {AppContainer} from './AppContainer'
import {Provider} from 'react-redux'
import {store} from './data/store'
import React from 'react'
import ReactDOM from 'react-dom'

/**
 * Render the application to the #fhir-root element.
 */
ReactDOM.render(
  <Provider store={store}>
    <AppContainer />
  </Provider>,
  document.getElementById('fhir-root')
)
