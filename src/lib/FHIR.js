const BASE_URL = 'https://fhir-open.sandboxcerner.com/dstu2/0b8a0111-e8e6-4c26-a91c-5069cbc6b1ca'

function _fetch (url, cb) {
  const opts = {
    headers: {
      'Accept': 'application/json-fhir'
    }
  }

  window.fetch(url, opts)
    .then(function (response) {
      return response.json()
    })
    .then(function (json) {
      cb(null, json)
    })
    .catch(function (err) {
      cb(err, null)
    })
}

export default {
  getPatientInfo (patientId, cb) {
    const url = `${BASE_URL}/Patient?_count=1&_id=${patientId}`
    if (!patientId) {
      cb(null, {})
    } else {
      _fetch(url, cb)
    }
  },

  getPatientConditions (patientId, cb) {
    const url = `${BASE_URL}/Condition?patient=${patientId}&clinicalstatus=active`
    _fetch(url, cb)
  }
}
