import {App} from './App'
import {connect} from 'react-redux'

function mapStateToProps (state) {
  return {
    conditionsError: state.conditionsError,
    conditionsLoading: state.conditionsLoading,
    error: state.error,
    loading: state.loading,
    patientData: state.patientData
  }
}

export const AppContainer = connect(mapStateToProps)(App)
