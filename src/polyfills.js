/** To support IE11 and a wider range of Android browsers */
import 'whatwg-fetch'
/** Needed by whatwg-fetch to further extend browser support */
import 'promise-polyfill'
