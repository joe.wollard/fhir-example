import {Alert, Container, Row, Col} from 'reactstrap'
import {ConditionsTableContainer} from './components/Conditions/TableContainer'
import {Loading} from './components/Loading'
import {PatientIDFieldContainer} from './components/PatientIDFieldContainer'
import {PatientInfoContainer} from './components/PatientInfoContainer'
import React from 'react'

/**
 * A simple container whose job it is to align the content to the center of the
 * viewport.
 */
export function App (props) {
  const {
    loading,
    error,
    conditionsError,
    conditionsLoading,
    patientData
  } = props

  return (
    <div style={{height: '100vh'}}>
      <Container>
        <Row>
          <Col>
            <h1 className='text-center'>Patient Info</h1>
          </Col>
        </Row>
        <Row>
          <Col sm={4}>
            <PatientIDFieldContainer />
            <br />
            {loading && <Loading />}
            {(!loading && patientData) && <PatientInfoContainer />}
            <Alert isOpen={Boolean(error)} color='danger'>
              {error && error.toString()}
            </Alert>
          </Col>
          <Col sm={8}>
            <Alert isOpen={Boolean(conditionsError)} color='danger'>
              {conditionsError && conditionsError.toString()}
            </Alert>
            {conditionsLoading && <Loading />}
            {!conditionsLoading &&
              <ConditionsTableContainer />
            }
          </Col>
        </Row>
      </Container>
    </div>
  )
}
